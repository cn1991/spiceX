import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SpiceProvider } from "../../providers/spice/spice";
import { LaunchpadDetails } from '../launchpad-details/launchpad-details';

@Component({
	selector: 'launchpads-list',
	templateUrl: 'launchpads-list.html',
})
export class LaunchpadsList {
	launchpads = new Array<any>();
	filter = new Array<any>();

	constructor(public navCtrl: NavController, private spiceProvider: SpiceProvider, public navParams: NavParams) {
		this.updateData();
	}

	getLaunchpadDetails(launchpad) {
		this.navCtrl.push(LaunchpadDetails, {
			launchpad: launchpad
		});
	}

	resetFilter() {
		this.filter = this.launchpads;
	}

	getFilter(event: any) {
		this.resetFilter();
		const val = event.target.value;
		if (val && val.trim() != '') {
			this.filter = this.filter.filter((item) => {
				return (item.full_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}

	updateData() {
		this.spiceProvider.getAllLaunchpads().subscribe(data => {
			this.launchpads = data;
			this.filter = data;
		})
	}

}
