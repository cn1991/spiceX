import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SpiceProvider } from "../../providers/spice/spice";
import { CoreDetails } from '../core-details/core-details';

@Component({
	selector: 'cores-list',
	templateUrl: 'cores-list.html',
})
export class CoresList {
	cores = new Array<any>();
	filter = new Array<any>();

	constructor(public navCtrl: NavController, private spiceProvider: SpiceProvider, public navParams: NavParams) {
		this.updateData();
	}

	getCoreDetails(core) {
		this.navCtrl.push(CoreDetails, {
			core: core
		});
	}

	resetFilter() {
		this.filter = this.cores;
	}

	getFilter(event: any) {
		this.resetFilter();
		const val = event.target.value;
		if (val && val.trim() != '') {
			this.filter = this.filter.filter((item) => {
				return (item.core_serial.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}

	updateData() {
		this.spiceProvider.getDetailledInfoForAllCores().subscribe(data => {
			this.cores = data;
			this.filter = data;
		})
	}

}
