import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
	selector: 'capsule-details',
	templateUrl: 'capsule-details.html'
})

export class CapsuleDetails {
	
	capsule: any;

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.capsule = navParams.get('capsule');
	}
}
